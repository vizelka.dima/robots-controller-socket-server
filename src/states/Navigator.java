package states;

import conv.Messages;
import conn.Robot;
import conv.Coords;
import conv.EDIR;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.net.Socket;

public class Navigator extends AState{
    private final Robot robot;

    public Navigator(Socket socket, BufferedReader reader, BufferedWriter writer, ESTATE nextState) {
        super(socket, reader, writer, nextState);
        hasNext = false;
        robot = new Robot();
    }


    private boolean detectInitialCoords(){
        if (!move())
            return false;

        if (robot.reachedTarget()) {
            getSecretMessage();
            return false;
        }

        if(!move())
            return false;

        if (robot.reachedTarget()){
            getSecretMessage();
            return false;
        }

        while(robot.getDir() == EDIR.UNKNOWN) {
            if (!turnLeft() || !move())
                return false;
        }
        return true;
    }

    private boolean getSecretMessage(){
        write(Messages.SERVER_PICK_UP);
        if (!read(Messages.CLIENT_MESSAGE_LEN))
            return false;
        write(Messages.SERVER_LOGOUT);
        return true;
    }

    @Override
    public void run() {
        if (detectInitialCoords() && navigate()) {
            getSecretMessage();
        }
    }


    private boolean move(){
        write(Messages.SERVER_MOVE);
        if(!read(Messages.CLIENT_OK_LEN))
            return false;

        if (!currentMsg.matches("OK [-+]?\\d+ [-+]?\\d+")){
            write(Messages.SERVER_SYNTAX_ERROR);
            return false;
        }

        String[] sp = currentMsg.split("\\s+");
        int x = 0;
        int y = 0;

        try{
            x = Integer.parseInt(sp[1]);
            y = Integer.parseInt(sp[2]);
        }catch (NumberFormatException e)
        {
            e.printStackTrace();
            write(Messages.SERVER_SYNTAX_ERROR);
            return false;
        }
        robot.setCoords(new Coords(x,y));
        System.out.println("COORDS = [" + x+ ", "+y +"]");
        return true;
    }

    private boolean turnLeft() {
        write(Messages.SERVER_TURN_LEFT);
        if(!read(Messages.CLIENT_OK_LEN))
            return false;
        robot.turnLeft();
        return true;
    }

    private boolean turnRight(){
        write(Messages.SERVER_TURN_RIGHT);
        if(!read(Messages.CLIENT_OK_LEN))
            return false;
        robot.turnRight();
        return true;
    }



    private boolean navigate() {
        while (!robot.isDirToTarget())
        { turnLeft(); }

        while (!robot.reachedTarget()) {
            if (robot.isMoving && robot.isDirToTarget()){
                if(!move())
                    return false;
            }
            else
            {
                if(robot.isTargetLeft()) {
                    if (!turnLeft())
                        return false;
                }
                else if(robot.isTargetRight()) {
                    if (!turnRight())
                        return false;
                }
                else {
                    if(!turnLeft() || !move())
                        return false;
                }
                if (robot.isDirToTarget()){
                    if(!move())
                        return false;
                }
            }
        }
        return true;
    }
}
