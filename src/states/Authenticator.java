package states;

import conv.Keys;
import conv.Messages;
import conv.Converter;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class Authenticator extends AState{
    private final List<Keys> keysList;
    private String username;

    public Authenticator(Socket socket,
                         BufferedReader reader,
                         BufferedWriter writer,
                         ESTATE nextState)
    {
        super(socket, reader, writer, nextState);
        keysList = new ArrayList<>();
        keysList.add(new Keys(0,23019,32037));
        keysList.add(new Keys(1,32037,29295));
        keysList.add(new Keys(2,18789,13603));
        keysList.add(new Keys(3,16443,29533));
        keysList.add(new Keys(4,18189,21952));
    }

    private boolean username(){
        if(!read(Messages.CLIENT_USERNAME_LEN))
            return false;
        username = currentMsg;
        return true;
    }

    private boolean keyId(){
        write(Messages.SERVER_KEY_REQUEST);
        if (!read(Messages.CLIENT_KEY_ID_LEN))
            return false;

        int keyId = 0;
        try{
            keyId = Integer.parseInt(currentMsg);
        }catch (NumberFormatException e)
        {
            write(Messages.SERVER_SYNTAX_ERROR);
            return false;
        }

        if (keyId < 0 || keyId >= keysList.size()){
            write(Messages.SERVER_KEY_OUT_OF_RANGE_ERROR);
            return false;
        }

        Keys keys = keysList.get(keyId);
        int serverKey = keys.SERVER_KEY;
        int clientKey = keys.CLIENT_KEY;

        int usernameHash = Converter.toHash(username);
        int code = Converter.code(usernameHash, serverKey);

        write(Integer.toString(code));


        if(!read(Messages.CLIENT_CONFIRMATION_LEN)) {
            return false;
        }

        String codeFromClient = currentMsg;

        int clientCode = 0;
        try{ clientCode = Integer.parseInt(codeFromClient); }
        catch (NumberFormatException e){
            write(Messages.SERVER_SYNTAX_ERROR);
            return false;
        }
        if (codeFromClient.length() > 5){
            write(Messages.SERVER_SYNTAX_ERROR);
            return false;
        }

        int clientHash = Converter.decode(clientCode, clientKey);

        if(usernameHash == clientHash)
        { write(Messages.SERVER_OK); }
        else
        {
            write(Messages.SERVER_LOGIN_FAILED);
            return false;
        }

        return true;
    }


    @Override
    public void run()
    {
        if (!username() || !keyId())
        { hasNext = false; }
    }
}
