package conn;

import states.AState;
import states.Authenticator;
import states.ESTATE;
import states.Navigator;

import java.io.*;
import java.net.Socket;
import java.net.SocketException;
import java.util.HashMap;
import java.util.Map;

public class Connection implements Runnable{
    private final Socket socket;
    BufferedWriter writer;
    BufferedReader reader;
    Map<ESTATE, AState> states;

    public Connection(Socket socket) {
        this.socket = socket;

        try {
            writer = new BufferedWriter(
                    new OutputStreamWriter(socket.getOutputStream()));

            reader = new BufferedReader(
                    new InputStreamReader(socket.getInputStream()));
        } catch (IOException e){
            e.printStackTrace();
        }

        states = new HashMap<>();
        states.put(ESTATE.AUTHENTICATE, new Authenticator(socket, reader, writer, ESTATE.NAVIGATE));
        states.put(ESTATE.NAVIGATE, new Navigator(socket, reader, writer, ESTATE.END));
    }

    @Override
    public void run() {
        try {
            socket.setSoTimeout(1000);
        } catch (SocketException e) {
            e.printStackTrace();
        }

        ESTATE currentState = ESTATE.AUTHENTICATE;
        AState state = states.get(currentState);
        while(true)
        {
            state.run();
            if (!state.hasNext())
                break;
            state = states.get(state.getNextState());
        }

        closeSocket();
    }

    private void closeSocket()
    {
        try { socket.close(); }
        catch (IOException e)
        { e.printStackTrace(); }
    }

}
