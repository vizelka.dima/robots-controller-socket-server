package conn;

import conv.Coords;
import conv.EDIR;

public class Robot {
    private boolean prevLocated;
    private boolean located;
    private Coords prevPos;
    private Coords currentPos;
    public boolean isMoving;
    private EDIR dir;

    public Robot()
    { this.located = false;
    prevLocated = false;
    dir=EDIR.UNKNOWN;}

    public void setCoords(Coords coords) {
        if (located) {
            prevPos = currentPos;
            currentPos = coords;
            detectDir();
            prevLocated = true;
        }
        currentPos = coords;
        isMoving = prevLocated && !prevPos.equals(currentPos);
        located = true;
    }

    private void detectDir() {
        if (prevPos.x < currentPos.x)
            dir = EDIR.RIGHT;
        else if (prevPos.x > currentPos.x)
            dir = EDIR.LEFT;
        else if (prevPos.y > currentPos.y)
            dir = EDIR.DOWN;
        else if (prevPos.y < currentPos.y)
            dir = EDIR.UP;
    }

    public EDIR getDir() {
        return dir;
    }

    public boolean reachedTarget()
    { return located && currentPos.x == 0 && currentPos.y == 0; }


    public void turnLeft() {
        dir = EDIR.left(dir);
    }

    public void turnRight() {
        dir = EDIR.right(dir);
    }

    public EDIR dirToTargetV(){
        if (currentPos.x > 0)
            return EDIR.LEFT;
        else if (currentPos.x < 0)
            return EDIR.RIGHT;
        return EDIR.REACHED;
    }
    public EDIR dirToTargetH(){
        if (currentPos.y > 0)
            return EDIR.DOWN;
        else if(currentPos.y < 0)
            return EDIR.UP;
        return EDIR.REACHED;
    }

    public boolean isDirToTarget() {
        return dirToTargetH() == dir || dirToTargetV() == dir;
    }

    public boolean isTargetRight() {
        EDIR toRight = EDIR.right(getDir());
        return toRight == dirToTargetV() || toRight == dirToTargetH();
    }

    public boolean isTargetLeft() {
        EDIR toLeft = EDIR.left(getDir());
        return toLeft == dirToTargetV() || toLeft == dirToTargetH();
    }
}
