package conv;

public class Messages {
    //conn.Server messages
    public static String SERVER_CONFIRMATION = ""; //<16-bitové číslo v decimální notaci>
    public static String SERVER_MOVE	     = "102 MOVE";
    public static String SERVER_TURN_LEFT	 = "103 TURN LEFT";
    public static String SERVER_TURN_RIGHT   = "104 TURN RIGHT";
    public static String SERVER_PICK_UP	     = "105 GET MESSAGE";
    public static String SERVER_LOGOUT       = "106 LOGOUT";
    public static String SERVER_KEY_REQUEST  = "107 KEY REQUEST";
    public static String SERVER_OK           = "200 OK";
    public static String SERVER_LOGIN_FAILED = "300 LOGIN FAILED";
    public static String SERVER_SYNTAX_ERROR = "301 SYNTAX ERROR";
    public static String SERVER_LOGIC_ERROR  = "302 LOGIC ERROR";
    public static String SERVER_KEY_OUT_OF_RANGE_ERROR = "303 KEY OUT OF RANGE";

    //Client messages LENGTH
    public static String CLIENT_OK           = "OK ";
    public static String CLIENT_RECHARGING   = "RECHARGING";
    public static String CLIENT_FULL_POWER   = "FULL POWER";

    public static final int CLIENT_USERNAME_LEN = 18;
    public static final int CLIENT_KEY_ID_LEN = 3;
    public static final int CLIENT_CONFIRMATION_LEN = 5;
    public static final int CLIENT_OK_LEN = 10;
    public static final int CLIENT_RECHARGING_LEN = 10;
    public static final int CLIENT_FULL_POWER_LEN = 10;
    public static final int CLIENT_MESSAGE_LEN = 98;

}
