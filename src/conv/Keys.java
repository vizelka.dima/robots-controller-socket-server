package conv;

public class Keys {
    public int KEY_ID;
    public int SERVER_KEY;
    public int CLIENT_KEY;

    public Keys(int key_id, int server_key, int client_key){
        this.KEY_ID = key_id;
        this.CLIENT_KEY = client_key;
        this.SERVER_KEY = server_key;
    }
}
